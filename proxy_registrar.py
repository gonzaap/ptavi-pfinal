import sys
import socketserver
import socket
import json
import time
from xml.sax import make_parser
from xml.sax.handler import ContentHandler


def log(msg, log_path):
    fichero = open(log_path, 'a')
    fichero.write(time.strftime('%Y-%m-%d :%S'))
    fichero.write(msg + '\r\n')
    fichero.close()


class ProxyHandler(ContentHandler):

    def __init__(self):

        self.lista = {}
        self.etiqueta_actual = ''
        self.diccionario = {'server': ['name', 'ip', 'puerto'],
                            'database': ['path', 'pswdpath'],
                            'log': ['']}

    def startElement(self, name, attrs):
        if name in self.diccionario:
            self.etiqueta_actual = name
            if name != 'log':
                d = {}
                for atributo in self.diccionario[name]:
                    d[atributo] = attrs.get(atributo, '')
                self.lista[name] = d

    def endElement(self, name):
        self.etiqueta_actual = ''

    def characters(self, content):
        if self.etiqueta_actual == 'log':
            self.lista[self.etiqueta_actual] = content

    def get_tags(self):
        return self.lista


class SIPRegisterHandler(socketserver.DatagramRequestHandler):

    dicc = {}

    def register2json(self):
        with open(datos_proxy['database']['path'], 'w') as fichero:
            json.dump(self.dicc, fichero, indent=4)

    def json2register(self):
        try:
            with open(datos_proxy['database']['path'], 'r') as jsonfile:
                self.dicc = json.load(jsonfile)
        except FileNotFoundError:
            pass

    def caducidad(self):
        hora = time.time() + 3600
        for correo_usuario in self.dicc.copy():
            caducidad = self.dicc[correo_usuario]['Dia'] + \
                        self.dicc[correo_usuario]['Caducidad']
            if hora >= caducidad:
                del self.dicc[correo_usuario]

    def datos_cliente(self,  ip, puerto, linea):
        self.wfile.write(bytes(linea, 'utf-8'))
        mensaje = linea.replace('\r\n', ' ')
        log('Sent to ' + ip + ':' + str(puerto) + ': ' + mensaje, LOGIN)

    def intercambio_cliente(self, destino, linea, ack=False):
        ip = self.dicc[destino]['DIRECCION']
        puerto = self.dicc[destino]['PUERTO']
        mensaje = linea.replace('\r\n', ' ')
        log('Sent to ' + ip + ':' + str(puerto) + ': ' + mensaje, LOGIN)
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
            my_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            my_socket.connect((ip, puerto))
            my_socket.send(bytes(linea, 'utf-8'))
            if not ack:
                data = my_socket.recv(1024).decode('utf-8')
                log('Received from ' + ip + ':' + str(puerto) +
                    ': ' + data.replace('\r\n', ' '), LOGIN)
                self.datos_cliente(self.client_address[0],
                                   self.client_address[1], data)

    def handle(self):
        linea_datos = self.rfile.read().decode('utf-8')
        IP = self.client_address[0]
        PUERTO = self.client_address[1]
        self.json2register()
        self.caducidad()
        metodo = linea_datos.split()[0]
        print(linea_datos)

        if linea_datos.split()[1].split(':')[0] != 'sip':
            self.datos_cliente(IP, PUERTO, 'SIP/2.0 400 Bad Request\r\n\r\n')
        else:

            if metodo == 'INVITE':
                correo_usuario1 = linea_datos.split()[1].split(':')[1]
                correo_usuario2 = linea_datos.split('\r\n')[5].split('=')[1].split()[0]
                if correo_usuario1 in self.dicc and correo_usuario2 in self.dicc:
                    self.intercambio_cliente(correo_usuario1, linea_datos)
                else:
                    self.datos_cliente(IP, PUERTO,
                                       'SIP/2.0 404 User Not Found\r\n\r\n')

            elif metodo == 'ACK':
                correo_usuario1 = linea_datos.split()[1].split(':')[1]
                if correo_usuario1 in self.dicc:
                    self.intercambio_cliente(correo_usuario1,
                                             linea_datos, True)
                else:
                    self.datos_cliente(IP, PUERTO,
                                       'SIP/2.0 404 User Not Found\r\n\r\n')

            elif metodo == 'BYE':
                correo_usuario1 = linea_datos.split()[1].split(':')[1]
                if correo_usuario1 in self.dicc:
                    self.intercambio_cliente(correo_usuario1, linea_datos)
                else:
                    self.datos_cliente(IP, PUERTO,
                                       'SIP/2.0 404 User Not Found\r\n\r\n')

            elif metodo == 'REGISTER':
                correo_usuario = linea_datos.split()[1].split(':')[1]
                puerto = linea_datos.split()[1].split(':')[2]
                caducidad = int(linea_datos.split('\r\n')[1].split(':')[1])
                hora = time.time() + 3600
                if caducidad == 0:
                    if correo_usuario in self.dicc:
                        LINE = 'SIP/2.0 200 OK\r\n\r\n'
                        del self.dicc[correo_usuario]
                    else:
                        LINE = 'SIP/2.0 404 User Not Found\r\n\r\n'
                else:
                    d = {}
                    d['DIRECCION'] = self.client_address[0]
                    d['PUERTO'] = int(puerto)
                    d['Dia'] = hora
                    d['Caducidad'] = caducidad
                    self.dicc[correo_usuario] = d
                    LINE = 'SIP/2.0 200 OK\r\n\r\n'
                self.datos_cliente(IP, PUERTO, LINE)
            else:
                self.datos_cliente(IP, PUERTO,
                                   'SIP/2.0 405 Method Not Allowed\r\n\r\n')
        self.register2json()


if __name__ == '__main__':

    xml_configuracion = sys.argv[1]
    parser = make_parser()
    xml_proxy = ProxyHandler()
    parser.setContentHandler(xml_proxy)
    parser.parse(open(xml_configuracion))
    datos_proxy = xml_proxy.get_tags()
    LOGIN = datos_proxy['log']
    ip = datos_proxy['server']['ip']
    puerto = int(datos_proxy['server']['puerto'])
    proxy = socketserver.UDPServer((ip, puerto), SIPRegisterHandler)
    print('servidor ' + datos_proxy['server']['name'] +
          ' en IP: ' + ip + ' PUERTO: ' + str(puerto))
    try:
        proxy.serve_forever()
    except KeyboardInterrupt:
        print('\nFin del servidor')
