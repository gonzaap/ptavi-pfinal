#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""
import sys
import socketserver
import simplertp
import secrets
from uaclient import SmallSMILHandler
from xml.sax import make_parser
import time
import random


def log(msg, log_path):
    fichero = open(log_path, 'a')
    fichero.write(time.strftime('%Y-%m-%d :%S'))
    fichero.write(msg + '\r\n')
    fichero.close()


class EchoHandler(socketserver.DatagramRequestHandler):
    """
    Echo server class
    """

    rtp_direccion = []

    def handle(self):
        # Escribe dirección y puerto del cliente (de tupla client_address)
        mensaje = self.rfile.read().decode('utf-8')
        print(mensaje)
        metodo = mensaje.split(' ')[0]
        if metodo in ['INVITE', 'ACK', 'BYE']:
            if mensaje.split(' ')[1].split(':')[0] != 'sip':
                self.wfile.write(b'SIP/2.0 400 Bad Request\r\n\r\n')

            else:

                if metodo == 'INVITE':
                    self.rtp_direccion.append(mensaje.split('\r\n')[5].split()[1])
                    self.rtp_direccion.append(int(mensaje.split('\r\n')[8].split()[1]))
                    self.wfile.write(b'SIP/2.0 100 Trying\r\n\r\n')
                    self.wfile.write(b'SIP/2.0 180 Ringing\r\n\r\n')
                    cuerpo_sdp = 'v=0\r\no=' + usuario + ' ' \
                                 + dir_ip + '\r\ns=alfombra\r\nt=0\r\nm=audio '
                    cuerpo_sdp += str(rtpaudio) + ' RTP'
                    LINE = 'SIP/2.0 200 OK\r\nContent-Type: Application/sdp\r\nContent-Length: '
                    LINE += str(len(cuerpo_sdp)) + '\r\n\r\n' + cuerpo_sdp + '\r\n\r\n'
                    self.wfile.write(bytes(LINE, 'utf-8'))

                elif metodo == 'BYE':
                    self.rtp_direccion = []
                    self.wfile.write(b'SIP/2.0 200 OK\r\n\r\n')
                else:
                    BIT = secrets.randbelow(1)
                    RTP_header = simplertp.RtpHeader()
                    csrc = [random.randint(2, 4) for i in range(25)]
                    RTP_header.set_header(version=2, marker=BIT,
                                          payload_type=14,
                                          cc=random.randint(0, 15))
                    RTP_header.setCSRC(csrc)
                    audio = simplertp.RtpPayloadMp3(propiedades['audio:path'])
                    simplertp.send_rtp_packet(RTP_header,
                                              audio, self.rtp_direccion[0],
                                              self.rtp_direccion[1])

        else:
            self.wfile.write(b'SIP/2.0 405 Method Not Allowed\r\n\r\n')


if __name__ == "__main__":

    parser = make_parser()
    cHandler = SmallSMILHandler()
    archivo = sys.argv[1]
    parser.setContentHandler(cHandler)

    try:
        parser.parse(open(archivo))
    except FileNotFoundError:
        sys.exit('File not found')
    propiedades = cHandler.get_tags()

    if propiedades['uaserver:ip'] == '':
        dir_ip = '127.0.0.1'

    else:
        dir_ip = propiedades['uaserver:ip']

    usuario = propiedades['account:username']
    rtpaudio = int(propiedades['rtpaudio:puerto'])
    puerto = int(propiedades['uaserver:puerto'])
    serv = socketserver.UDPServer((dir_ip, puerto), EchoHandler)
    print("Escuchando...")
    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        sys.exit('Finalizando el servidor')
