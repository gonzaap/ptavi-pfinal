import sys
import socket
from xml.sax import make_parser
from xml.sax.handler import ContentHandler
import time
import secrets
import random
import simplertp


class SmallSMILHandler(ContentHandler):

    def __init__(self):
        self.tags = {'account': ['username', 'passwd'],
                     'uaserver': ['ip', 'puerto'],
                     'rtpaudio': ['puerto'],
                     'regproxy': ['ip', 'puerto'],
                     'log': ['path'],
                     'audio': ['path']}
        self.list = {}

    def startElement(self, name, atrb):

        if name in self.tags:
            for atributo in self.tags[name]:
                self.list[name + ":" + atributo] = atrb.get(atributo, "")

    def get_tags(self):
        return self.list


def log(msg, log_path):
    fichero = open(log_path, 'a')
    fichero.write(time.strftime('%Y-%m-%d :%S'))
    fichero.write(msg + '\r\n')
    fichero.close()


def respuesta_invite(data):
    trying = 'SIP/2.0 100 Trying' in data
    ringing = 'SIP/2.0 180 Ringing' in data
    ok = 'SIP/2.0 200 OK' in data
    return trying and ringing and ok


def coger_datos_rtp(data):
    a, b = data.find('o='), data.find('s=')
    ip_rtp = data[a:b].split()[1].replace('\r\n', '')
    c, d = data.find('m='), data.find('RTP')
    puerto_rtp = int(data[c:d].split()[1])
    return ip_rtp, puerto_rtp


if __name__ == "__main__":

    try:
        archivo = sys.argv[1]
        metodo = sys.argv[2]
        opcion = sys.argv[3]

    except (IndexError, ValueError):
        sys.exit('Usage: uaclient.py config metodo opcion')

    parser = make_parser()
    cHandler = SmallSMILHandler()
    parser.setContentHandler(cHandler)

    try:
        parser.parse(open(archivo))
    except FileNotFoundError:
        sys.exit('Archivo no encontrado')
    propiedades = cHandler.get_tags()

    if propiedades['uaserver:ip'] == '':
        dir_ip = '127.0.0.1'
    else:
        dir_ip = propiedades['uaserver:ip']

    if propiedades['regproxy:ip'] == ' ':
        proxyip = '127.0.0.1'

    else:
        proxyip = propiedades['regproxy:ip']

    usuario = propiedades['account:username']
    puerto = int(propiedades['uaserver:puerto'])
    rtpaudio = int(propiedades['rtpaudio:puerto'])
    proxyport = int(propiedades['regproxy:puerto'])
    LOGIN = propiedades['log:path']

    # Creamos el socket, lo configuramos y lo atamos a un servidor/puerto
    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
        my_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        my_socket.connect((proxyip, proxyport))
        log("Starting...", LOGIN)

        if metodo == 'INVITE':

            cuerpo_sdp = 'v=0\r\no=' + usuario + ' ' + dir_ip
            cuerpo_sdp += '\r\ns=alfombra\r\nt=0\r\nm=audio ' \
                          + str(rtpaudio) + ' RTP'
            LINE = 'INVITE sip:' + opcion \
                   + ' SIP/2.0\r\nContent-Type: Application/sdp\r\nContent-Length: '
            LINE += str(len(cuerpo_sdp)) + '\r\n\r\n' + cuerpo_sdp + '\r\n\r\n'

        elif metodo == 'REGISTER':

            LINE = 'REGISTER sip:' + usuario + ':' + str(puerto) \
                   + ' SIP/2.0\r\n'
            LINE += 'Expires: ' + opcion + '\r\n\r\n'

        elif metodo == 'BYE':

            LINE = 'BYE sip:' + opcion + ' SIP/2.0\r\n\r\n'

        my_socket.send(bytes(LINE, 'utf-8'))
        log(' Sent to ' + proxyip + ':' + str(proxyport) + ': ' + LINE, LOGIN)
        data = my_socket.recv(1024).decode('utf-8')
        log(' Received from ' + proxyip + ':'
            + str(proxyport) + ': ' + data, LOGIN)
        print('Recibido -- ', data)

        if respuesta_invite(data):
            LINE = 'ACK sip:' + opcion + ' SIP/2.0\r\n\r\n'
            my_socket.send(bytes(LINE, 'utf-8'))
            log(' Sent to ' + proxyip + ':'
                + str(proxyport) + ': ' + LINE, LOGIN)
            ip_rtp, puerto_rtp = coger_datos_rtp(data)
            BIT = secrets.randbelow(1)
            RTP_header = simplertp.RtpHeader()
            csrc = [random.randint(2, 4) for i in range(25)]
            RTP_header.set_header(version=2, marker=BIT,
                                  payload_type=14, cc=random.randint(0, 15))
            RTP_header.setCSRC(csrc)
            audio = simplertp.RtpPayloadMp3(propiedades['audio:path'])
            simplertp.send_rtp_packet(RTP_header, audio, ip_rtp, puerto_rtp)

    print("Terminando socket...")
    print("Fin.")
    log("Finishing...", LOGIN)
